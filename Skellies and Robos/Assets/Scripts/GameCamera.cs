﻿using UnityEngine;
using System.Collections;

public class GameCamera: MonoBehaviour {
	
	private Transform target;

	private Vector3 cameraTarget;

	// Use this for initialization
	void Start () {
		// Takes up a lot of resources. Only execute ONCE.
		// Points to the player's transform.
		target = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		// Set camera target 
		cameraTarget = new Vector3(target.position.x, transform.position.y, target.position.z);
		// Smooth out movement of camera
		transform.position = Vector3.Lerp(transform.position, cameraTarget, Time.deltaTime * 8);
	}
}
