﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {

	public Transform experienceBar;
	public TextMesh levelText;

	public void SetPlayerExperience (float percentToLevel, int playerLevel) {
		levelText.text = "Level: " + playerLevel;
		experienceBar.localScale = new Vector3(percentToLevel, experienceBar.localScale.y, experienceBar.localScale.z);
	}
}
