﻿using UnityEngine;
using System.Collections;

// Automatically adds component
[RequireComponent (typeof (CharacterController))]
public class PlayerController : MonoBehaviour {

	// Component Variables
	private CharacterController controller;
	private Camera cam;
	public Gun gun;
	//Animator in video 4

	// System Variables
	private Quaternion targetRotation;

	//Handling Variables
	public float rotationSpeed = 450f;
	public float walkSpeed = 5f;
	public float runSpeed = 8f;
	private float acceleration = 5f;
	private Vector3 currentVelocityMod;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController>();
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		//ControlWASD();
		ControlMouse();
		if(Input.GetButtonDown("Shoot")) {
			gun.Shoot();
		}
		else if (Input.GetButton("Shoot")) {
			gun.ShootContinuous();
		}
	}

	void ControlMouse() {
		// Returns mouse on screen position
		Vector3 mousePos = Input.mousePosition;
		// Converts mousePos to world space coordinates and the Z is the distance between the camera and player
		mousePos = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.y - transform.position.y));
		// Subtract to make rotation relative to player instead of center of world
		targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x, 0, transform.position.z));
		// Quaterntion to Vector3
		// Look towards roation over time
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);

		Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

		currentVelocityMod = Vector3.MoveTowards(currentVelocityMod, input, acceleration * Time.deltaTime); 
		Vector3 motion = currentVelocityMod;
		// If moving diagonal, change 1.4 speed to 1 by multiplying by 0.7
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
		// If running, multiply by runSpeed, otherwise multiply by walkSpeed
		motion *= (Input.GetButton("Run")) ? runSpeed : walkSpeed;
		// Applies Gravity
		motion += Vector3.up * -8;
		
		// "Move" Detects for collisions
		controller.Move(motion * Time.deltaTime);
	}

	void ControlWASD() {
		Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
		
		// Takes direction and looks toward it.
		if(input != Vector3.zero) {
			targetRotation = Quaternion.LookRotation(input);
			// Quaterntion to Vector3
			// Look towards roation over time
			transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);
		}
		currentVelocityMod = Vector3.MoveTowards(currentVelocityMod, input, acceleration * Time.deltaTime); 
		Vector3 motion = currentVelocityMod;
		// If moving diagonal, change 1.4 speed to 1 by multiplying by 0.7
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
		// If running, multiply by runSpeed, otherwise multiply by walkSpeed
		motion *= (Input.GetButton("Run")) ? runSpeed : walkSpeed;
		// Applies Gravity
		motion += Vector3.up * -8;
		
		// "Move" Detects for collisions
		controller.Move(motion * Time.deltaTime);
	}
}
