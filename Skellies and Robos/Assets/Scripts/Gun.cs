﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class Gun : MonoBehaviour {

	public LayerMask collisionMask;

	public enum GunType {
		SEMI,
		BURST,
		AUTO
	};

	public GunType gunType;
	public float rpm;
	public float damage = 1;

	// Component Variables
	public Transform spawn;
	private LineRenderer tracer;

	// System Variables
	private float secondsBetweenShots;
	private float nextPossibleShootTime;

	void Start () {
		secondsBetweenShots = 60/rpm;

		if(GetComponent<LineRenderer>()) {
			tracer = GetComponent<LineRenderer>();
		}

	}

	public void Shoot () {

		if(CanShoot()) {
			// Create a ray from spawn to it's forward direction
			Ray ray = new Ray(spawn.position, spawn.forward);
			RaycastHit hit;

			// Range of shot
			float shotDistance = 20f;

			if(Physics.Raycast(ray, out hit, shotDistance, collisionMask)) {
				shotDistance = hit.distance; // Set distance to distance between spawn and hit

				if(hit.collider.GetComponent<Entity>()) {
					hit.collider.GetComponent<Entity>().TakeDamage(damage);
				}
			}

			//Debug.DrawRay(ray.origin, ray.direction * shotDistance, Color.red, 1);

			nextPossibleShootTime = Time.time + secondsBetweenShots;

			GetComponent<AudioSource>().Play();

			if(tracer) {
				StartCoroutine("RenderTracer", ray.direction * shotDistance);
			}
		}
	}

	public void ShootContinuous () {
		if(gunType == GunType.AUTO) {
			Shoot ();
		}
	}

	private bool CanShoot() {
		bool canShoot = true;

		if(Time.time < nextPossibleShootTime)
			canShoot = false;

		return canShoot;
	}

	IEnumerator RenderTracer (Vector3 hitPoint) {
		tracer.enabled = true;
		tracer.SetPosition(0, spawn.position);
		tracer.SetPosition(1, spawn.position + hitPoint);
		yield return null;
		tracer.enabled = false;
	}
}
